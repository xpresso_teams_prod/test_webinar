import os
import sys
import types
import numpy
import keras
import pickle
import pandas
import marshal
import seaborn
import autofeat
import warnings
import matplotlib
import tensorflow
from numpy import load
import matplotlib.pyplot
from xgboost import XGBRegressor
from keras.models import Sequential
from sklearn.impute import SimpleImputer
from keras.layers import Dense, Dropout, LSTM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from autofeat import FeatureSelector, AutoFeatRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, QuantileTransformer
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_file = pickle.load(open(f"{PICKLE_PATH}/train_file.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    create_training_and_test_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/create_training_and_test_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    normalization = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/normalization.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_extraction = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_extraction.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_cleaning = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_cleaning.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    feature_engineering = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/feature_engineering.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = feature_engineering
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["train_file"]
## $xpr_param_global_methods = ["create_training_and_test_data", "normalization","data_extraction","data_cleaning","feature_engineering"]



warnings.filterwarnings("ignore")
def feature_engineering(dataframe):
    dataframe_description = dataframe.describe()
    df_plot = dataframe.copy()
    df_corr = df_plot.corr(method='pearson')
    fig, ax = matplotlib.pyplot.subplots(figsize=(10,10))
    axes = seaborn.heatmap(df_corr, linewidths=.2)
    axes = dataframe_description.T.plot.bar(subplots=True, figsize=(20,15))
    data = dataframe.drop(['cycle','id'], axis=1)
    
    X = (data.iloc[:,:-1]).to_numpy()
    y = (data.iloc[:,-1]).to_numpy()
    
    #Creating training and test data
    X_train, X_test, y_train, y_test = create_training_and_test_data(X, y)
    
    #Normalizing train and test data
    X_train_normalized, X_test_normalized = normalization(X_train, X_test)
    
    #Feature Engineering using autofeat
    afreg = AutoFeatRegressor(verbose=1, feateng_steps = 1, featsel_runs = 1)
    X_train_transformed = afreg.fit_transform(X_train_normalized, y_train)
    X_test_transformed = afreg.transform(X_test_normalized)
    print("R^2 after feature engineering: %.4f" % afreg.score(X_test_transformed, y_test))  
    X_train_transformed = X_train_transformed.to_numpy()
    X_test_transformed = X_test_transformed.to_numpy()
    return X_train_transformed, X_test_transformed, y_train, y_test

try:
    pickle.dump(marshal.dumps(create_training_and_test_data.__code__), open(f"{PICKLE_PATH}/create_training_and_test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(normalization.__code__), open(f"{PICKLE_PATH}/normalization.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_extraction.__code__), open(f"{PICKLE_PATH}/data_extraction.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_cleaning.__code__), open(f"{PICKLE_PATH}/data_cleaning.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(feature_engineering.__code__), open(f"{PICKLE_PATH}/feature_engineering.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(train_file, open(f"{PICKLE_PATH}/train_file.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

